<?php
/**
 * Plugin Name: Wiki Content Sync
 * Plugin URI:
 * Description:
 * Version: 1.0
 * Author:
 * Author URI:
 * License:
 */


/*
// Ping
../?ping=true&wcs_access_key=...

// Show Basic Posts Datas
../?show_basic_posts_datas=true&wcs_access_key=...

// Show Selected Post Datas
../?show_selected_post_datas=true&post_hash=ffd8be6787711f2cd7124902bad09780&wcs_access_key=...

// Get All Terms
../?get_all_terms=true&wcs_access_key=...

// Regenerate Posts Hash
../?regenerate_posts_hash=true&wcs_access_key=...

// Regenerate Terms Hash
../?regenerate_terms_hash=true&wcs_access_key=...

// Check Post exist
../?check_post_exist=true&post_hash=ffd8be6787711f2cd7124902bad09780&wcs_access_key=...

// Clear Diff. Site Posts Cache
../?clear_diff_site_posts_cache=true&wcs_access_key=...

// Get post's featured image
../?get_featured_image=true&post_hash=ffd8be6787711f2cd7124902bad09780&wcs_access_key=...
*/


 class Wiki_Content_Sync {

	const TERM_BLENDER = 1329;
	const DIFF_SITE_POSTS_CACHE_LIFETIME = 1800; // 30 min
	protected $plugin_dir_path, $plugin_dir_url;
	private $actions;


 	public function __construct() {


 		/********************************/
 		/*     SET GLOBAL VARIABLES     */
 		/********************************/

 		$this->plugin_dir_path = plugin_dir_path( __FILE__ );
 		$this->plugin_dir_url = plugin_dir_url( __FILE__ );

 		/* --- */


 		/***************************/
 		/*     BASIC FUNCTIONS     */
 		/***************************/

 		$this->requireClasses( $this->plugin_dir_path .'includes/functions.php' );
 		$this->functions = new WCS_Functions($this);

		$this->requireClasses( $this->plugin_dir_path .'includes/functions/simple_diff.php' );
 		$this->functionsSimpleDiff = new WCS_Functions_SimpleDiff($this);

 		/* --- */


 		/***********************/
 		/*     ADD ACTIONS     */
 		/***********************/

 		$this->requireClasses( $this->plugin_dir_path .'includes/actions.php' );
 		$this->actions = new WCS_Actions($this);

		$this->requireClasses( $this->plugin_dir_path .'includes/actions/task_manager.php' );
 		$this->actionsTaskManager = new WCS_Actions_taskManager($this);

		$this->requireClasses( $this->plugin_dir_path .'includes/actions/ajax.php' );
 		$this->actionsAjax = new WCS_Actions_Ajax($this);


 		// STYLES & SCRIPTS
 		add_action('admin_enqueue_scripts', array($this, 'admin_enqueue_scripts_function'));

		// Define Editor Page
		add_action( 'admin_menu', array($this->actions, 'SyncMenuSettingsMenuItem') );

		// Admin Notices
		add_action( 'admin_notices', array($this->actions, 'postSynchronizationStatus') );


		// --- Task Manager ---

		// Ping
		add_action( 'init', array($this->actionsTaskManager, 'ping') );

		// Get Basic Posts Datas
		add_action( 'init', array($this->actionsTaskManager, 'showBasicPostsDatas') );

		// Get Selected Post Datas
		add_action( 'init', array($this->actionsTaskManager, 'showSelectedPostDatas') );

		// Get All Terms
		add_action( 'init', array($this->actionsTaskManager, 'getAllTerms') );

		// Regenerate Terms Hash
		add_action( 'init', array($this->actionsTaskManager, 'regenerateTermsHash') );

		// Regenerate Posts Hash
		add_action( 'init', array($this->actionsTaskManager, 'regeneratePostsHash') );

		// Check Post Exist
		add_action( 'init', array($this->actionsTaskManager, 'checkPostExist') );

		// Clear Diff. Site Posts Cache
		add_action( 'init', array($this->actionsTaskManager, 'clearDiffSitePostsCache') );

		// Get post's featured image
		add_action( 'init', array($this->actionsTaskManager, 'getFeaturedImage') );

		// --- /Task Manager ---


		// AJAX
		add_action( 'wp_ajax_get_post_datas_with_ajax', array($this->actionsAjax, 'get_post_datas_with_ajax') );
		add_action( 'wp_ajax_nopriv_get_post_datas_with_ajax', array($this->actionsAjax, 'get_post_datas_with_ajax') );

		add_action( 'wp_ajax_start_sync_with_ajax', array($this->actionsAjax, 'start_sync_with_ajax') );
		add_action( 'wp_ajax_nopriv_start_sync_with_ajax', array($this->actionsAjax, 'start_sync_with_ajax') );

		add_action( 'wp_ajax_skip_sync_with_ajax', array($this->actionsAjax, 'skip_sync_with_ajax') );
		add_action( 'wp_ajax_nopriv_skip_sync_with_ajax', array($this->actionsAjax, 'skip_sync_with_ajax') );

 		/* --- */


 		/***********************/
 		/*     ADD FILTERS     */
 		/***********************/



 		/* --- */


		/**********************/
		/*     META BOXES     */
		/**********************/

		$this->requireClasses( $this->plugin_dir_path .'includes/meta_boxes.php' );
		$this->meta_boxes = new WCS_MetaBoxes($this);

		$this->requireClasses( $this->plugin_dir_path .'includes/save_post_datas.php' );
		$this->save_post_datas = new WCS_SavePostDatas($this);

		// Settings Metabox
		add_action( 'add_meta_boxes', array($this->meta_boxes, 'SettingsMetabox') );

		// Save Post Data
		add_action( 'save_post', array($this->save_post_datas, 'SavePostdatas') );

		/* --- */

 	}


 	/***************************/
 	/*     OTHER FUNCTIONS     */
 	/***************************/

 	// Include file by path
 	function requireClasses( $filePath = "" ) {
 		if ( !empty($filePath) && file_exists($filePath) ) { require_once( $filePath ); }
 	}
 	function getPluginDirPath()	{ return $this->plugin_dir_path; }
 	function getPluginDirUrl()	{ return $this->plugin_dir_url; }

 	/* --- */


 	/****************************/
 	/*     STYLES & SCRIPTS     */
 	/****************************/

 	function admin_enqueue_scripts_function($hook) {
 		$screen = get_current_screen();

 		if ( $screen->base == 'toplevel_page_wcs_sync_page' ) {
 			wp_register_script('wcs_admin_js', $this->plugin_dir_url .'js/admin.js');
 			wp_enqueue_script('wcs_admin_js');
			wp_localize_script('wcs_admin_js', 'wcs', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));

 			wp_enqueue_style('wcs_admin_css', $this->plugin_dir_url .'css/admin.css', array());
 		}
 	}

 	/* --- */

 }

 new Wiki_Content_Sync();
