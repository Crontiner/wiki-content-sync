<?php

/**********************/
/*     META BOXES     */
/**********************/

class WCS_MetaBoxes {

	public function __construct($wcs) {
		$this->wcs = $wcs;
	}

	function SettingsMetabox($post) {
		add_meta_box(	'wcs_settings',
									'Sync settings',
									array($this, 'SettingsMetaboxFunction'),
									$this->wcs->functions->get_allowed_cpts(),
									'side',
									'high'
								);
	}

	function SettingsMetaboxFunction($post) {
		$sync_disabled = get_post_meta( $post->ID, 'wcs_sync_disabled', true );

		?>
			<table>
				<tr>
					<td><input type="checkbox" value="1" name="sync_disabled" id="sync_disabled" <?php checked($sync_disabled, 1); ?> /></td>
					<th><label for="sync_disabled">Sync letiltva</label></th>
				</tr>
			</table>
		<?php

	}

}
