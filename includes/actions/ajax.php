<?php

/****************/
/*     AJAX     */
/****************/

class WCS_Actions_Ajax {

	public function __construct($wcs) {
		$this->wcs = $wcs;
	}


	public function get_post_datas_with_ajax() {
		$this_site_post_hash = $_POST['this_site_post_hash'];
		$other_site_post_hash = $_POST['other_site_post_hash'];
		$duplicate = strtolower($_POST['duplicate']);
		$site = $_POST['site']; // this || other
		$result_html = "";

		$this_site_datas = "";
		$other_site_datas = "";
		$allowed_taxonomies = $this->wcs->functions->get_allowed_taxonomies();

		if ( $duplicate == 'true' ) { $duplicate = TRUE; }
		else { $duplicate = FALSE; }


		// Get this site data
		if ( $site == 'this' ) {
			if ( $duplicate === TRUE ) {
				$this_site_datas = $this->wcs->functions->get_post_data($this_site_post_hash);
				$other_site_datas = $this->wcs->functions->get_post_datas_from_other_site($other_site_post_hash);
			} else {
				$this_site_datas = $this->wcs->functions->get_post_data($this_site_post_hash);
			}
		}

		// Get other site data
		if ( $site == 'other' ) {
			if ( $duplicate === TRUE ) {
				$this_site_datas = $this->wcs->functions->get_post_data($this_site_post_hash);
				$other_site_datas = $this->wcs->functions->get_post_datas_from_other_site($other_site_post_hash);
			} else {
				$other_site_datas = $this->wcs->functions->get_post_datas_from_other_site($other_site_post_hash);
			}
		}


		$this_site_datas = $this->wcs->functions->remove_unused_terms_data_from_post_data($this_site_datas);
		$other_site_datas = $this->wcs->functions->remove_unused_terms_data_from_post_data($other_site_datas);


		// Find different values
		if ( !empty($this_site_datas) && !empty($other_site_datas) ) {
			foreach ($this_site_datas as $key => $val) {
				$this_site_datas[$key .'_class'] = "";
				$other_site_datas[$key .'_class'] = "";

				if ( $duplicate === TRUE ) {

					$this_site_datas_temp = $this_site_datas[$key];
					if ( is_array($this_site_datas_temp) ) { $this_site_datas_temp = serialize($this_site_datas_temp); }
					$this_site_datas_temp = md5(base64_encode($this_site_datas_temp));

					$other_site_datas_temp = $other_site_datas[$key];
					if ( is_array($other_site_datas_temp) ) { $other_site_datas_temp = serialize($other_site_datas_temp); }
					$other_site_datas_temp = md5(base64_encode($other_site_datas_temp));

					if ( $this_site_datas_temp != $other_site_datas_temp ) {
						$this_site_datas[$key .'_class'] = 'difference';
						$other_site_datas[$key .'_class'] = 'difference';

						// marking changes
						if ( $site == 'this' ) {
							//$this_site_datas[$key] = $this->wcs->functionsSimpleDiff->htmlDiff( nl2br($other_site_datas[$key]), nl2br($this_site_datas[$key]) );
						}
						else if ( $site == 'other' ) {
							//$other_site_datas[$key] = $this->wcs->functionsSimpleDiff->htmlDiff( nl2br($this_site_datas[$key]), nl2br($other_site_datas[$key]) );
						}

					}
				}
			}
		}


		if ( !empty($this_site_datas) ) {

			$this_site_taxonomies_tr = "";
			foreach ($allowed_taxonomies as $key => $taxonomy) {
				$this_site_taxonomies_tr .=
					'<tr class="'. $this_site_datas['term_'. $taxonomy .'_class'] .'">'.
						'<th>'. $taxonomy .'</th>'.
						'<td>'. serialize($this_site_datas['term_'. $taxonomy]) .'</td>'.
					'</tr>';
			}

			$this_site_datas =
				'<table>'.
					'<tr><th colspan="2" class="main_header">'. $this->wcs->functions->get_this_site_url() .'</th></tr>'.
					'<tr class="'. $this_site_datas['title_class'] .'">'.
						'<th>title</th>'.
						'<td><a href="'. $this_site_datas['post_edit_link'] .'" target="_blank">'. $this_site_datas['title'] .'</a></td>'.
					'</tr>'.
					'<tr class="'. $this_site_datas['slug_class'] .'">'.
						'<th>slug</th>'.
						'<td>'. $this_site_datas['slug'] .'</td>'.
					'</tr>'.
					'<tr class="'. $this_site_datas['content_class'] .'">'.
						'<th>content</th>'.
						'<td>'. $this_site_datas['content'] .'</td>'.
					'</tr>'.
					'<tr class="'. $this_site_datas['has_post_thumbnail_class'] .'">'.
						'<th>has_post_thumbnail</th>'.
						'<td>'. json_encode($this_site_datas['has_post_thumbnail']) .'</td>'.
					'</tr>'.
					$this_site_taxonomies_tr.
					'<tr class="'. $this_site_datas['post_modified_date_class'] .'">'.
						'<th>post_modified_date</th>'.
						'<td>'. date('Y-m-d H:i:s', $this_site_datas['post_modified_date']) .'</td>'.
					'</tr>'.
					'<tr class="'. $this_site_datas['post_created_date_class'] .'">'.
						'<th>post_created_date</th>'.
						'<td>'. date('Y-m-d H:i:s', $this_site_datas['post_created_date']) .'</td>'.
					'</tr>'.
				'</table>';

			//$this_site_datas .= '<div class="compressed_data">'. base64_encode($this_site_datas) .'</div>';
			$this_site_datas = '<div class="this_site_datas" data-post-modified-date="'. $this_site_datas['post_modified_date'] .'">'. $this_site_datas .'</div>';
		}


		if ( !empty($other_site_datas) ) {

			$other_site_taxonomies_tr = "";
			foreach ($allowed_taxonomies as $key => $taxonomy) {
				$other_site_taxonomies_tr .=
					'<tr class="'. $other_site_datas['term_'. $taxonomy .'_class'] .'">'.
						'<th>'. $taxonomy .'</th>'.
						'<td>'. serialize($other_site_datas['term_'. $taxonomy]) .'</td>'.
					'</tr>';
			}

			$other_site_datas =
				'<table>'.
					'<tr><th colspan="2" class="main_header">'. $this->wcs->functions->get_other_site_url() .'</th></tr>'.
					'<tr class="'. $other_site_datas['title_class'] .'">'.
						'<th>title</th>'.
						'<td><a href="'. $other_site_datas['post_edit_link'] .'" target="_blank">'. $other_site_datas['title'] .'</a></td>'.
					'</tr>'.
					'<tr class="'. $other_site_datas['slug_class'] .'">'.
						'<th>slug</th>'.
						'<td>'. $other_site_datas['slug'] .'</td>'.
					'</tr>'.
					'<tr class="'. $other_site_datas['content_class'] .'">'.
						'<th>content</th>'.
						'<td>'. $other_site_datas['content'] .'</td>'.
					'</tr>'.
					'<tr class="'. $other_site_datas['has_post_thumbnail_class'] .'">'.
						'<th>has_post_thumbnail</th>'.
						'<td>'. json_encode($other_site_datas['has_post_thumbnail']) .'</td>'.
					'</tr>'.
					$other_site_taxonomies_tr.
					'<tr class="'. $other_site_datas['post_modified_date_class'] .'">'.
						'<th>post_modified_date</th>'.
						'<td>'. date('Y-m-d H:i:s', $other_site_datas['post_modified_date']) .'</td>'.
					'</tr>'.
					'<tr class="'. $other_site_datas['post_created_date_class'] .'">'.
						'<th>post_created_date</th>'.
						'<td>'. date('Y-m-d H:i:s', $other_site_datas['post_created_date']) .'</td>'.
					'</tr>'.
				'</table>';

			//$other_site_datas .= '<div class="compressed_data">'. base64_encode($other_site_datas) .'</div>';
			$other_site_datas = '<div class="other_site_datas" data-post-modified-date="'. $other_site_datas['post_modified_date'] .'">'. $other_site_datas .'</div>';
		}

		$result_html = 	$this_site_datas . $other_site_datas .'<div class="clear"></div>';
		if ( $site == 'other' ) {

			if ( $duplicate === TRUE ) {
				$result_html .= '<button class="skip_sync button button-default button-large">Skip This Sync</button>';
			}

			$result_html .= '<button class="start_sync button button-default button-large">Start Sync From '. $this->wcs->functions->get_other_site_url() .'</button>';
			$result_html .= '<br>';
		}

		echo json_encode(array('html' => $result_html, 'ajax_hash' => $_POST['ajax_hash']));
		exit;
	}


	public function start_sync_with_ajax() {
		$this_site_post_hash = $_POST['this_site_post_hash'];
		$other_site_post_hash = $_POST['other_site_post_hash'];
		$duplicate = strtolower($_POST['duplicate']);
		$site = $_POST['site']; // this || other
		$result = FALSE;

		$this_site_datas = "";
		$other_site_datas = "";

		if ( $duplicate == 'true' ) { $duplicate = TRUE; }
		else { $duplicate = FALSE; }


		// Get this site data
		if ( $site == 'this' ) {
			if ( $duplicate === TRUE ) {
				$this_site_datas = $this->wcs->functions->get_post_data($this_site_post_hash);
				$other_site_datas = $this->wcs->functions->get_post_datas_from_other_site($other_site_post_hash);
			} else {
				$this_site_datas = $this->wcs->functions->get_post_data($this_site_post_hash);
			}
		}

		// Get other site data
		if ( $site == 'other' ) {
			if ( $duplicate === TRUE ) {
				$this_site_datas = $this->wcs->functions->get_post_data($this_site_post_hash);
				$other_site_datas = $this->wcs->functions->get_post_datas_from_other_site($other_site_post_hash);
			} else {
				$other_site_datas = $this->wcs->functions->get_post_datas_from_other_site($other_site_post_hash);
			}
		}


		if ( $site == 'this' ) {
			$to_hash = "";
			if ( isset($other_site_datas['wcs_post_data_hash']) ) { $to_hash = $other_site_datas['wcs_post_data_hash']; }
			//$result = (bool) $this->wcs->functions->insert_or_update_post( $this_site_datas, $to_hash );
		}

		if ( $site == 'other' ) {
			$to_hash = "";
			if ( isset($this_site_datas['wcs_post_data_hash']) ) { $to_hash = $this_site_datas['wcs_post_data_hash']; }
			$result = (bool) $this->wcs->functions->insert_or_update_post( $other_site_datas, $to_hash );
		}


		echo json_encode(array('result' => $result, 'ajax_hash' => $_POST['ajax_hash']));
		exit;
	}


	public function skip_sync_with_ajax() {
		$this_site_post_hash = $_POST['this_site_post_hash'];
		$other_site_post_hash = $_POST['other_site_post_hash'];
		$duplicate = strtolower($_POST['duplicate']);
		$site = $_POST['site']; // this || other
		$result = FALSE;

		$this_site_datas = "";
		$other_site_datas = "";

		if ( $duplicate == 'true' ) { $duplicate = TRUE; }
		else { $duplicate = FALSE; }


		// Get this site data
		if ( $site == 'this' ) {
			if ( $duplicate === TRUE ) {
				$this_site_datas = $this->wcs->functions->get_post_data($this_site_post_hash);
				$other_site_datas = $this->wcs->functions->get_post_datas_from_other_site($other_site_post_hash);
			} else {
				$this_site_datas = $this->wcs->functions->get_post_data($this_site_post_hash);
			}
		}

		// Get other site data
		if ( $site == 'other' ) {
			if ( $duplicate === TRUE ) {
				$this_site_datas = $this->wcs->functions->get_post_data($this_site_post_hash);
				$other_site_datas = $this->wcs->functions->get_post_datas_from_other_site($other_site_post_hash);
			} else {
				$other_site_datas = $this->wcs->functions->get_post_datas_from_other_site($other_site_post_hash);
			}
		}


		if ( $site == 'this' ) {
			$to_hash = "";
			if ( isset($other_site_datas['wcs_post_data_hash']) ) { $to_hash = $other_site_datas['wcs_post_data_hash']; }
			//$result = (bool) $this->wcs->functions->insert_or_update_post( $this_site_datas, $to_hash );
		}

		if ( $site == 'other' ) {
			$to_hash = "";
			if ( isset($this_site_datas['wcs_post_data_hash']) ) { $to_hash = $this_site_datas['wcs_post_data_hash']; }

			if ( $duplicate === TRUE ) {
				$post_ID = $this->wcs->functions->get_post_id_by_post_hash( $to_hash );
				if ( intval($post_ID) > 0 ) {
					$result = (bool) $this->wcs->functions->update_post_modified_date($other_site_datas, $post_ID);
				}
			}
		}


		echo json_encode(array('result' => $result, 'ajax_hash' => $_POST['ajax_hash']));
		exit;
	}


}
