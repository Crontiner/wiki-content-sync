<?php

/***********************/
/*     ADD ACTIONS     */
/***********************/

class WCS_Actions_taskManager {

	public function __construct($wcs) {
		$this->wcs = $wcs;
	}


	// Ping
	public function ping() {

		if ( isset($_GET['ping']) ) {
			if ( $this->wcs->functions->access_key_validation() === TRUE ) {
				$result = array('result' => 'pong');

				echo base64_encode(serialize($result));
				die;
			}
		}
	}


	// Check Post is exist
	public function checkPostExist() {

		if ( isset($_GET['check_post_exist']) && isset($_GET['post_hash']) && !empty($_GET['post_hash']) ) {
			if ( $this->wcs->functions->access_key_validation() === TRUE ) {

				$result = (bool) $this->wcs->functions->is_post_exists_by_post_hash($_GET['post_hash']);
				$result = array('exist' => $result);

				echo base64_encode(serialize($result));
				die;
			}
		}
	}


	// Get Basic Posts Datas
	public function showBasicPostsDatas() {

		if ( isset($_GET['show_basic_posts_datas']) ) {
			if ( $this->wcs->functions->access_key_validation() === TRUE ) {
				$basic_posts_datas = $this->wcs->functions->get_basic_posts_datas();
				echo base64_encode(serialize($basic_posts_datas));
				die;
			}
		}
	}


	// Get Selected Post Datas
	public function showSelectedPostDatas() {

		if ( isset($_GET['show_selected_post_datas']) ) {
			if ( $this->wcs->functions->access_key_validation() === TRUE ) {

				if (isset($_GET['post_hash']) && !empty($_GET['post_hash'])) { $post_id = $_GET['post_hash']; }
				if (isset($_GET['post_id']) && intval($_GET['post_id']) > 0) { $post_id = intval($_GET['post_id']); }

				if ( !empty($post_id) ) {
					$basic_post_datas = $this->wcs->functions->get_post_data($post_id);
					echo base64_encode(serialize($basic_post_datas));
					die;
				}
			}
		}
	}


	// Get All Terms
	public function getAllTerms() {

		if ( isset($_GET['get_all_terms']) ) {
			if ( $this->wcs->functions->access_key_validation() === TRUE ) {

				$terms_datas = $this->wcs->functions->get_terms_datas();
				echo base64_encode(serialize($terms_datas));
				die;
			}
		}
	}


	// Regenerate Terms Hash
	public function regenerateTermsHash() {

		if ( isset($_GET['regenerate_terms_hash']) ) {
			if ( $this->wcs->functions->access_key_validation() === TRUE ) {

				$result = (bool) $this->wcs->functions->generate_terms_hash();
				$result = array('ready' => $result);

				echo base64_encode(serialize($result));
				die;
			}
		}
	}


	// Regenerate Posts Hash
	public function regeneratePostsHash() {

		if ( isset($_GET['regenerate_posts_hash']) ) {
			if ( $this->wcs->functions->access_key_validation() === TRUE ) {

				$result = (bool) $this->wcs->functions->generate_posts_hash();
				$result = array('ready' => $result);

				echo base64_encode(serialize($result));
				die;
			}
		}
	}


	// Clear Diff. Site Posts Cache
	public function clearDiffSitePostsCache() {

		if ( isset($_GET['clear_diff_site_posts_cache']) ) {
			if ( $this->wcs->functions->access_key_validation() === TRUE ) {

				$this->wcs->functions->clear_diff_site_posts_cache();
				$result = array('ready' => TRUE);

				echo base64_encode(serialize($result));
				die;
			}
		}
	}


	// Get post's featured image
	public function getFeaturedImage() {

		if ( isset($_GET['get_featured_image']) && isset($_GET['post_hash']) && !empty($_GET['post_hash']) ) {
			if ( $this->wcs->functions->access_key_validation() === TRUE ) {

				$result = $this->wcs->functions->get_featured_image_by_post_id($_GET['post_hash']);
				$result = array('result' => $result);

				echo base64_encode(serialize($result));
				die;
			}
		}
	}


}
