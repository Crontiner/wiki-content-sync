<?php

/********************************/
/*     SAVE POST META DATAS     */
/********************************/

class WCS_SavePostDatas {

	public function __construct($wcs) {
		$this->wcs = $wcs;
	}

	function SavePostdatas($post_id) {
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) { return ""; }
		global $post;
		$screen = get_current_screen();

		if ( in_array($screen->post_type, $this->wcs->functions->get_allowed_cpts()) ) {

			update_post_meta( $post->ID, 'wcs_sync_disabled', intval($_POST['sync_disabled']) );

		}
	}

}
