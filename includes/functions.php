<?php

/***************************/
/*     BASIC FUNCTIONS     */
/***************************/

class WCS_Functions {

	public function __construct($wcs) {
		$this->wcs = $wcs;
	}


	function get_allowed_cpts() {
		return array('post', 'page', 'ftp_kapcsolatok_cpt', 'wp_adminok_cpt', 'useful_plugins_cpt');
	}


	function get_allowed_taxonomies() {
		return array('category', 'post_tag', 'useful_plugins_category');
	}


	// Update 'posts' table 'post_modified' col
	function update_post_modified_date($post_data = "", $post_ID = "") {
		if ( intval($post_ID) > 0 ) { $post_ID = intval($post_ID); }
		else { return FALSE; }
		if ( is_array($post_data) && !empty($post_data) ) {  }
		else { return FALSE; }
		if ( isset($post_data['post_modified_date']) && isset($post_data['post_modified_date_gmt']) ) {  }
		else { return FALSE; }
		global $wpdb;

		$row_id_array = $this->wcs->functions->get_row_id_by_post_id($post_ID);

		if ( isset($row_id_array['last_revision_id']) && (intval($row_id_array['last_revision_id']) > 0) ) {
			$wpdb->update(
				$wpdb->prefix .'posts',
				array('post_modified' => date('Y-m-d H:i:s', $post_data['post_modified_date']),
							'post_modified_gmt' => date('Y-m-d H:i:s', $post_data['post_modified_date_gmt']),),
				array( 'ID' => $row_id_array['last_revision_id'] ),
				array( '%s','%s' ),
				array( '%d' )
			);
		}
		if ( isset($row_id_array['first_id']) && (intval($row_id_array['first_id']) > 0) ) {
			$wpdb->update(
				$wpdb->prefix .'posts',
				array('post_modified' => date('Y-m-d H:i:s', $post_data['post_modified_date']),
							'post_modified_gmt' => date('Y-m-d H:i:s', $post_data['post_modified_date_gmt']),),
				array( 'ID' => $row_id_array['first_id'] ),
				array( '%s','%s' ),
				array( '%d' )
			);
		}

		return TRUE;
	}


	// Get 'posts' table row ID
	function get_row_id_by_post_id($post_ID = "") {
		if ( intval($post_ID) > 0 ) { }
		else { return FALSE; }
		global $wpdb;
		$result = array( 'row_id' => 0, 'first_id' => 0, 'last_revision_id' => 0 );

		/*
		$row_obj = $wpdb->get_row( "	SELECT `ID`, `post_parent` FROM `{$wpdb->prefix}posts`
																	WHERE `post_parent` = {$post_ID}
																	ORDER BY `post_modified` DESC" );

		if ( isset($row_obj->ID) && (intval($row_obj->ID) > 0) ) { return intval($row_obj->ID); }
		else {
			$row_obj = $wpdb->get_row( "	SELECT `ID` FROM `{$wpdb->prefix}posts`
																		WHERE `ID` = {$post_ID}
																		ORDER BY `post_modified` DESC" );

			if ( isset($row_obj->ID) && (intval($row_obj->ID) > 0) ) { return intval($row_obj->ID); }
		}
		return FALSE;
		*/

		$row_obj = $wpdb->get_row( "	SELECT `ID` FROM `{$wpdb->prefix}posts`
																	WHERE `ID` = {$post_ID}
																	ORDER BY `post_modified` DESC" );

		if ( isset($row_obj->ID) && (intval($row_obj->ID) > 0) ) {
			$result['row_id'] = intval($row_obj->ID);
			$result['first_id'] = intval($row_obj->ID);
		}

		$row_obj = $wpdb->get_row( "	SELECT `ID`, `post_parent` FROM `{$wpdb->prefix}posts`
																	WHERE `post_parent` = {$post_ID}
																	ORDER BY `post_modified` DESC" );

		if ( isset($row_obj->ID) && (intval($row_obj->ID) > 0) ) {
			$result['row_id'] = intval($row_obj->ID);
			$result['last_revision_id'] = intval($row_obj->ID);
		}

		return $result;
	}


	// return: all taxonomies terms
	function get_terms_datas() {
		$this_site_url = $this->wcs->functions->get_this_site_url();
		if ( $this_site_url !== FALSE ) { }
		else { $this_site_url = ""; }

		$categories = $this->wcs->functions->get_allowed_taxonomies();
		$categories_temp = array();

		foreach ($categories as $key => $taxonomy) {
			$terms_obj = get_terms($taxonomy, array('hide_empty' => false));

			foreach ($terms_obj as $key => $term_obj) {
				if ( isset($term_obj->name) && ($term_obj->term_id != Wiki_Content_Sync::TERM_BLENDER) ) {
					if ( $term_obj->parent != Wiki_Content_Sync::TERM_BLENDER ) {

						$parent_term_slug = "";
						if ( intval($term_obj->parent) > 0 ) {
							$parent_term_obj = get_term( $term_obj->parent, $term_obj->taxonomy );
							if ( isset($parent_term_obj->slug) ) {
								$parent_term_slug = $parent_term_obj->slug;
							}
						}

						$term_hash = get_term_meta( $term_obj->term_id, 'wcs_term_data_hash', true);
						$term_hash_timestamp = get_term_meta( $term_obj->term_id, 'wcs_term_data_hash_timestamp', true);

						$categories_temp[$taxonomy][] = array(
																									'term_id' => $term_obj->term_id,
																									'term_name' => $term_obj->name,
																									'term_slug' => $term_obj->slug,
																									'parent_term_slug' => $parent_term_slug,
																									'term_hash' => $term_hash,
																									'term_hash_timestamp' => $term_hash_timestamp,
																									'term_edit_link' => $this_site_url .'/wp-admin/term.php?taxonomy='. $taxonomy .'&tag_ID='. $term_obj->term_id,
																							);
					}
				}
			}
		}

		if ( !empty($categories_temp) ) { return $categories_temp; }
		return FALSE;
	}


	function insert_or_update_post($post_data = "", $to_hash = "") {
		if ( empty($post_data) ) { return FALSE; }

		if ( isset($post_data['title']) && isset($post_data['slug']) && isset($post_data['content']) ) {  }
		else { return FALSE; }


		// Get & Merge Terms
		$allowed_taxonomies = $this->wcs->functions->get_allowed_taxonomies();
		$merged_terms_array = array();

		foreach ($allowed_taxonomies as $key => $taxonomy) {
			if ( isset($post_data['term_'. $taxonomy]) && !empty($post_data['term_'. $taxonomy]) ) {
				$post_data['term_'. $taxonomy] = json_decode(json_encode($post_data['term_'. $taxonomy]), true);
				foreach ($post_data['term_'. $taxonomy] as $key => $term_array) {
					if ( !empty($term_array) ) {
						$merged_terms_array []= $term_array;
					}
				}
			}
		}
		// /Get & Merge Terms


		$post_ID = $this->wcs->functions->get_post_id_by_post_hash($to_hash);

		$post_data['content'] = html_entity_decode($post_data['content']);
		//$post_data['content'] = wp_specialchars_decode($post_data['content']);
		$post_data['content'] = str_replace(	$this->wcs->functions->get_other_site_url(),
																					$this->wcs->functions->get_this_site_url(),
																					$post_data['content'] );

		$post_args = array(
			'ID'								=> intval($post_ID),
			'post_title'				=> $post_data['title'],
			'post_name'					=> $post_data['slug'],
			'post_content'			=> $post_data['content'],
			'post_modified'			=> date('Y-m-d H:i:s', $post_data['post_modified_date']),
			'post_modified_gmt'	=> date('Y-m-d H:i:s', $post_data['post_modified_date_gmt']),
			'post_date'					=> date('Y-m-d H:i:s', $post_data['post_date']),
			'post_date_gmt'			=> date('Y-m-d H:i:s', $post_data['post_date_gmt']),
			'post_status'				=> 'publish',
			'post_type'					=> $post_data['post_type'],
		);


		// INSERT || UPDATE POST
		if ( intval($post_ID) > 0 ) {
			// update post
			wp_update_post( $post_args );
		} else {
			// insert post
			$post_ID = wp_insert_post( $post_args );
		}
		update_post_meta($post_ID, 'wcs_last_sync_timestamp', time());


		// INSERT AND SET TERMS OR JUST SET TERMS
		if ( intval($post_ID) > 0 ) {

			$only_set_post_terms_array = array();
			$insert_terms_array = array();

			foreach ($merged_terms_array as $key => $term_array) {
				if ( isset($term_array['slug']) ) {
					$term_obj = get_term_by('slug', $term_array['slug'], $term_array['taxonomy']);

					if ( isset($term_obj->term_id) ) {
						$term_id = intval($term_obj->term_id);
						$term_name = $term_obj->name;
						$taxonomy = $term_obj->taxonomy;

						$term = term_exists($term_name, $taxonomy);
						if ($term !== 0 && $term !== null) {

							// term is isset
							$only_set_post_terms_array[$taxonomy][]= $term_id;
						}
					} else {

						if ( isset($term_array['name']) && !empty($term_array['name']) ) {

							// set new term
							$new_term_data = wp_insert_term(
																							$term_array['name'], // the term
																							$term_array['taxonomy'] // the taxonomy
																						);
							$insert_terms_array[$taxonomy][]= $new_term_data['term_id'];
						}
					}
				}
			}

			if ( !empty($only_set_post_terms_array) ) {
				foreach ($only_set_post_terms_array as $taxonomy => $terms_array) {
					wp_set_post_terms( $post_ID, $terms_array, $taxonomy );
				}
			}

			if ( !empty($insert_terms_array) ) {
				foreach ($insert_terms_array as $taxonomy => $terms_array) {
					wp_set_post_terms( $post_ID, $terms_array, $taxonomy );
				}
			}
		}

		$result = $this->wcs->functions->update_post_modified_date($post_data, $post_ID);
		//var_dump($result); die;

		if ( intval($post_ID) > 0 ) { return TRUE; }
		return FALSE;
	}


	function clear_diff_site_posts_cache() {
		update_option( 'wcs_diff_site_posts_cache', "", false );
		update_option( 'wcs_diff_site_terms_cache', "", false );
	}


	function get_post_id_by_post_hash($post_hash = "") {
		if ( !empty($post_hash) ) {
			$all_posts = new WP_Query(array('post_type' => $this->wcs->functions->get_allowed_cpts(),
																			'fields' => 'ids',
																			'posts_per_page' => 1,
																			'meta_key'     => 'wcs_post_data_hash',
																			'meta_value'   => $post_hash,
																			'meta_compare' => '='
																			));
			$all_posts_array = $all_posts->posts;

			if ( intval($all_posts_array[0]) > 0 ) { return intval($all_posts_array[0]); }
		}
		return FALSE;
	}


	function is_post_exists_by_post_hash($post_hash = "") {
		if ( !empty($post_hash) ) {
			$all_posts = new WP_Query(array('post_type' => $this->wcs->functions->get_allowed_cpts(),
																			'fields' => 'ids',
																			'posts_per_page' => 1,
																			'meta_key'     => 'wcs_post_data_hash',
																			'meta_value'   => $post_hash,
																			'meta_compare' => '='
																			));
			$all_posts_array = $all_posts->posts;

			if ( intval($all_posts_array[0]) > 0 ) { return TRUE; }
		}
		return FALSE;
	}


	function array_sort_by_column(&$arr = "", $col = "", $dir = SORT_ASC) {
		$sort_col = array();
		foreach ($arr as $key=> $row) {
			$sort_col[$key] = $row[$col];
		}

		array_multisort($sort_col, $dir, $arr);
	}


	function get_this_site_url() {
		$wcs_settings = get_option('wcs_settings');

		if ( isset($wcs_settings['site1']) && !empty($wcs_settings['site1']) ) {
			if (strpos($wcs_settings['site1'], $_SERVER['HTTP_HOST']) !== false) { return $wcs_settings['site1']; }
		}
		if ( isset($wcs_settings['site2']) && !empty($wcs_settings['site2']) ) {
			if (strpos($wcs_settings['site2'], $_SERVER['HTTP_HOST']) !== false) { return $wcs_settings['site2']; }
		}

		return FALSE;
	}


	function get_other_site_url() {
		$wcs_settings = get_option('wcs_settings');

		if ( isset($wcs_settings['site1']) && !empty($wcs_settings['site1']) ) {
			if (strpos($wcs_settings['site1'], $_SERVER['HTTP_HOST']) !== false) {  }
			else { return $wcs_settings['site1']; }
		}
		if ( isset($wcs_settings['site2']) && !empty($wcs_settings['site2']) ) {
			if (strpos($wcs_settings['site2'], $_SERVER['HTTP_HOST']) !== false) {  }
			else { return $wcs_settings['site2']; }
		}

		return FALSE;
	}


	function get_access_key() {
		$wcs_settings = get_option('wcs_settings');
		if ( isset($wcs_settings['access_key']) && !empty($wcs_settings['access_key']) ) {
			return $wcs_settings['access_key'];
		}
		return "";
	}


	function access_key_validation() {
		if ( isset($_GET['wcs_access_key']) && !empty($_GET['wcs_access_key']) ) {
			$wcs_settings = get_option('wcs_settings');
			if ( isset($wcs_settings['access_key']) && !empty($wcs_settings['access_key']) ) {
				if ( $_GET['wcs_access_key'] == $wcs_settings['access_key'] ) {
					return TRUE;
				}
			}
		}
		return FALSE;
	}


	function get_the_content_by_id($post_id = "", $format = FALSE) {
		if ( intval($post_id) > 0 ) {  }
		else { return FALSE; }
		$page_data = get_page($post_id);

		if ( $page_data ) {

			if ( $format === TRUE ) {
				return apply_filters('the_content',$page_data->post_content);
			} else {
				return $page_data->post_content;
			}
		}
		return FALSE;
	}


	function get_post_data($post_id = "") {

		// if post_id is slug
		if ( !is_numeric($post_id) && !empty($post_id) ) {
			$post_slug = $post_id;
			$all_posts = new WP_Query(array('post_type' => $this->wcs->functions->get_allowed_cpts(),
																			'fields' => 'ids',
																			'posts_per_page' => 1,
																			'name' => $post_slug,
																			));
			$all_posts_array = $all_posts->posts;
			if ( intval($all_posts_array[0]) > 0 ) {
				$post_id = intval($all_posts_array[0]);
			}
		}

		// if post_id is hash
		if ( !is_numeric($post_id) && !empty($post_id) ) {
			$post_hash = $post_id;
			$all_posts = new WP_Query(array('post_type' => $this->wcs->functions->get_allowed_cpts(),
																			'fields' => 'ids',
																			'posts_per_page' => 1,
																			'meta_key'     => 'wcs_post_data_hash',
																			'meta_value'   => $post_hash,
																			'meta_compare' => '='
																			));
			$all_posts_array = $all_posts->posts;
			if ( intval($all_posts_array[0]) > 0 ) {
				$post_id = intval($all_posts_array[0]);
			}
		}

		if ( intval($post_id) > 0 ) {  }
		else { return FALSE; }

		if ( $this->wcs->functions->post_is_protected($post_id) === TRUE ) { return FALSE; }

		$this_site_url = $this->wcs->functions->get_this_site_url();
		$has_post_thumbnail = has_post_thumbnail($post_id);


		// Get thumbnail file content
		$thumbnail_file_content = "";
		if ( $has_post_thumbnail === TRUE ) {
			$thumbnail_file_content = $this->wcs->functions->get_featured_image_by_post_id($post_id);
			if ( ($thumbnail_file_content !== FALSE) && !empty($thumbnail_file_content) ) { /* ok */ }
			else { $thumbnail_file_content = ""; }
		}


		$post_data = array(	'title' 									=> get_the_title($post_id),
												'slug' 										=> get_post_field( 'post_name', $post_id ),
												'content' 								=> get_post_field('post_content', $post_id),
												'has_post_thumbnail' 			=> $has_post_thumbnail,
												'thumbnail_file_content'	=> $thumbnail_file_content,
												'post_type'								=> get_post_type($post_id),
												'post_date' 							=> get_post_time('U', false, $post_id),
												'post_date_gmt' 					=> get_post_time('U', true, $post_id),
												'post_modified_date' 			=> get_post_modified_time('U', false, $post_id),
												'post_modified_date_gmt' 	=> get_post_modified_time('U', true, $post_id),
												'post_created_date' 			=> get_the_date('U', $post_id),
												'post_edit_link' 					=> $this_site_url .'/wp-admin/post.php?post='. $post_id .'&action=edit',
												'wcs_post_data_hash' 			=> get_post_meta($post_id, 'wcs_post_data_hash', true),
											);

		// Set terms to array
		$allowed_taxonomies = $this->wcs->functions->get_allowed_taxonomies();
		foreach ($allowed_taxonomies as $key => $taxonomy) {
			$post_data['term_'. $taxonomy] = wp_get_post_terms($post_id, array($taxonomy), array("fields" => "all"));
		}

		return $post_data;
	}


	function generate_terms_hash() {
		$allowed_taxonomies = $this->wcs->functions->get_allowed_taxonomies();
		$categories = $this->wcs->functions->get_terms_datas();

		if ( !empty($categories) ) {
			foreach ($allowed_taxonomies as $key => $taxonomy) {
				if ( isset($categories[$taxonomy]) && !empty($categories[$taxonomy]) ) {
					foreach ($categories[$taxonomy] as $key => $term_array) {
						if ( isset($term_array['term_id']) ) {
							$term_id = $term_array['term_id'];
							$term_hash = md5( $term_array['term_name'] . $term_array['term_slug'] . $term_array['parent_term_slug'] );
							$term_hash = substr($term_hash, 0, 15);

							if ( $term_array['term_hash'] != $term_hash ) {
								update_term_meta($term_id, 'wcs_term_data_hash', $term_hash);
								update_term_meta($term_id, 'wcs_term_data_hash_timestamp', strtotime('NOW') );
							}
						}
					}
				}
			}
		}

		return TRUE;
	}


	function generate_posts_hash() {
		global $post;

		$all_posts = new WP_Query(array('post_type' => $this->wcs->functions->get_allowed_cpts(),
																		'fields' => 'ids',
																		'posts_per_page' => -1,
																		));
		$all_posts_array = $all_posts->posts;


		foreach ($all_posts_array as $key => $post_id) {
			if ( $this->wcs->functions->post_is_protected($post_id) === FALSE ) {
				$post_data = $this->wcs->functions->get_post_data($post_id);
				$post_data = $this->wcs->functions->remove_unused_terms_data_from_post_data($post_data);

				if ( isset($post_data['post_modified_date']) ) {
					unset($post_data['post_modified_date']);
				}

				if (!empty($post_data)) {
					$post_hash = md5(serialize($post_data));

					if ( get_post_meta($post_id, 'wcs_post_data_hash', true) != $post_hash ) {
						update_post_meta($post_id, 'wcs_post_data_hash', $post_hash);
						update_post_meta($post_id, 'wcs_post_data_hash_timestamp', strtotime('NOW') );
					}
				}
			}
		}

		return TRUE;
	}


	function get_basic_posts_datas() {
		global $post;

		$all_posts = new WP_Query(array('post_type' => $this->wcs->functions->get_allowed_cpts(),
																		'fields' => 'ids',
																		'posts_per_page' => -1,
																		));
		$all_posts_array = $all_posts->posts;

		$post_datas = array();
		foreach ($all_posts_array as $key => $post_id) {
			if ( $this->wcs->functions->post_is_protected($post_id) === FALSE ) {
				$post_type = get_post_type($post_id);

				$post_datas[$post_type][] = array('slug' => get_post_field( 'post_name', $post_id ),
																					'post_id' => $post_id,
																					'post_title' => get_the_title($post_id),
																					'wcs_post_data_hash' => get_post_meta($post_id, 'wcs_post_data_hash', true),
																					'post_modified_date' => get_post_modified_time('U', false, $post_id),
																					'post_type' => $post_type,
																			);
			}
		}

		return $post_datas;
	}


	function post_is_protected($post_id = "") {
		if ( intval($post_id) > 0 ) {  }
		else { return FALSE; }

		$sync_disabled = get_post_meta( $post_id, 'wcs_sync_disabled', true );
		if ( intval($sync_disabled) == 1 ) { return TRUE; }

		if ( get_post_type($post_id) == 'post' ) {
			$terms_obj = wp_get_post_terms($post_id, 'category', array("fields" => "all"));

			foreach ($terms_obj as $key => $term_obj) {
				if ( intval($term_obj->term_id) == Wiki_Content_Sync::TERM_BLENDER ) { return TRUE; }
				else if ( intval($term_obj->parent) == Wiki_Content_Sync::TERM_BLENDER ) { return TRUE; }
			}
		}

		return FALSE;
	}


	function get_last_synchronized_posts_table() {
		$last_sync_posts = new WP_Query(array(
																		'post_type' => $this->wcs->functions->get_allowed_cpts(),
																		'fields' => 'ids',
																		'posts_per_page' => 10,

																		'meta_key'   => 'wcs_last_sync_timestamp',
																		'orderby'    => 'meta_value_num',
																		'order'      => 'DESC',

																		'meta_query' => array(
																			array(
																				'key'     => 'wcs_last_sync_timestamp',
																				'value'   => "",
																				'compare' => '!=',
																			),
																		),
																	));
		$last_sync_posts_array = $last_sync_posts->posts;

		$last_sync_posts_table = "";
		$last_sync_posts_tr =
			'<tr><th colspan="4" class="main_header">Last synced posts</th></tr>'.
			'<tr>'.
				'<th>Title</th>'.
				'<th>Slug</th>'.
				'<th>Post Type</th>'.
				'<th>Last Sync Date</th>'.
			'</tr>';

		if ( !empty($last_sync_posts_array) ) {
			foreach ($last_sync_posts_array as $key => $post_id) {
				$last_sync_posts_tr .=
					'<tr>'.
						'<td><a href="'. get_permalink($post_id) .'" target="_blank">'. get_the_title($post_id) .'</a></td>'.
						'<td>'. get_post_field( 'post_name', $post_id ) .'</td>'.
						'<td>'. get_post_type($post_id) .'</td>'.
						'<td>'. date('Y-m-d H:i:s', get_post_meta($post_id, 'wcs_last_sync_timestamp', true) ) .'</td>'.
					'</tr>';
			}

			$last_sync_posts_table =
				'<table>'.
					$last_sync_posts_tr.
				'</table>'.
				'<div class="clear"></div><br>';

		} else { $last_sync_posts_table = ""; }

		return $last_sync_posts_table;
	}


	function get_modified_and_new_categories_table() {

		// Get cache
		$wcs_diff_site_terms_cache = get_option( 'wcs_diff_site_terms_cache' );
		if ( !empty($wcs_diff_site_terms_cache) ) {
			$wcs_diff_site_terms_cache = unserialize(base64_decode($wcs_diff_site_terms_cache));

			if ( isset($wcs_diff_site_terms_cache['timestamp']) ) {
				if ( time() - intval($wcs_diff_site_terms_cache['timestamp']) < Wiki_Content_Sync::DIFF_SITE_POSTS_CACHE_LIFETIME ) {
					$wcs_diff_site_terms_cache = $wcs_diff_site_terms_cache['result'];
				} else { $wcs_diff_site_terms_cache = ""; }
			} else { $wcs_diff_site_terms_cache = ""; }
		}

		if ( empty($wcs_diff_site_terms_cache) ) {

			$allowed_taxonomies = $this->wcs->functions->get_allowed_taxonomies();
			$this_site_terms = $this->wcs->functions->get_terms_datas();
			$other_site_terms = $this->wcs->functions->get_all_terms_from_other_site();

			$sites_terms = array();

			if ( !empty($this_site_terms) && is_array($this_site_terms) ) {
				foreach ($allowed_taxonomies as $key => $taxonomy) {
					foreach ($this_site_terms[$taxonomy] as $key => $term_array) {
						if ( isset($term_array['term_slug']) ) {
							$term_array['site'] = 'this';
							$term_array['taxonomy'] = $taxonomy;
							$sites_terms[$taxonomy][$term_array['term_slug']][]= $term_array;
						}
					}
				}
			}

			if ( !empty($other_site_terms) && is_array($other_site_terms) ) {
				foreach ($allowed_taxonomies as $key => $taxonomy) {
					foreach ($other_site_terms[$taxonomy] as $key => $term_array) {
						if ( isset($term_array['term_slug']) ) {
							$term_array['site'] = 'other';
							$term_array['taxonomy'] = $taxonomy;
							$sites_terms[$taxonomy][$term_array['term_slug']][]= $term_array;
						}
					}
				}
			}


			// Remove not changed terms
			if ( !empty($sites_terms) && is_array($sites_terms) ) {
				foreach ($sites_terms as $taxonomy => $terms_array) {

					foreach ($sites_terms[$taxonomy] as $term_slug => $terms_array2) {
						if ( count($terms_array2) > 1 ) {
							if ( $sites_terms[$taxonomy][$term_slug][0]['term_hash'] == $sites_terms[$taxonomy][$term_slug][1]['term_hash'] ) {
								unset($sites_terms[$taxonomy][$term_slug]);
							} else {

								$sites_terms[$taxonomy][$term_slug][0]['duplicate'] = TRUE;
								$sites_terms[$taxonomy][$term_slug][1]['duplicate'] = TRUE;

								// remove old version
								if ( intval($sites_terms[$taxonomy][$term_slug][0]['term_hash_timestamp']) > intval($sites_terms[$taxonomy][$term_slug][1]['term_hash_timestamp']) ) {
									unset($sites_terms[$taxonomy][$term_slug][1]);
									$sites_terms[$taxonomy][$term_slug] = array_values($sites_terms[$taxonomy][$term_slug]);
								} else if ( intval($sites_terms[$taxonomy][$term_slug][0]['term_hash_timestamp']) < intval($sites_terms[$taxonomy][$term_slug][1]['term_hash_timestamp']) ) {
									unset($sites_terms[$taxonomy][$term_slug][0]);
									$sites_terms[$taxonomy][$term_slug] = array_values($sites_terms[$taxonomy][$term_slug]);
								} else {
									unset($sites_terms[$taxonomy][$term_slug]);
								}
							}
						} else {
							$sites_terms[$taxonomy][$term_slug][0]['duplicate'] = FALSE;
						}
					}
				}
			}


			// Sort by term_hash_timestamp
			$sites_terms_temp = array();
			if ( !empty($sites_terms) && is_array($sites_terms) ) {
				foreach ($sites_terms as $taxonomy => $val) {
					foreach ($sites_terms[$taxonomy] as $term_slug => $terms_array) {
						$sites_terms_temp []= $terms_array[0];
					}
				}
			}
			$this->wcs->functions->array_sort_by_column($sites_terms_temp, 'term_hash_timestamp');
			$sites_terms = array_reverse($sites_terms_temp);
			unset($sites_terms_temp);


			// Set Cache
			update_option( 'wcs_diff_site_terms_cache', base64_encode(serialize(array( 'result' => $sites_terms, 'timestamp' => time() ))), false );

		} else {
			$sites_terms = $wcs_diff_site_terms_cache;
		}


		// Create Table
		$sites_terms_tr = "";
		foreach ($sites_terms as $key => $terms_array) {
			$sites_terms_tr .=
				'<tr>'.
					'<td>'. $terms_array['taxonomy'] .'</td>'.
					'<td><a href="'. $terms_array['term_edit_link'] .'" target="_blank">'. $terms_array['term_name'] .'</a></td>'.
					'<td>'. $terms_array['term_slug'] .'</td>'.
					'<td>'. $terms_array['parent_term_slug'] .'</td>'.
					'<td>'. date('Y-m-d H:i:s', $terms_array['term_hash_timestamp']) .'</td>'.
					'<td>'. strtoupper($terms_array['site']) .'</td>'.
					'<td>'. strtoupper(json_encode($terms_array['duplicate'])) .'</td>'.
				'</tr>';
		}
		$sites_terms_table =
			'<table>'.
				'<tr><th colspan="7" class="main_header">Modified & new terms/tags - ('. count($sites_terms) .' DB)</th></tr>'.
				'<tr>'.
					'<th>Taxonomy</th>'.
					'<th>Term Name</th>'.
					'<th>Term Slug</th>'.
					'<th>Parent Term Slug</th>'.
					'<th>Term Hash Date</th>'.
					'<th>Site</th>'.
					'<th>Duplicate</th>'.
				'</tr>'.
				$sites_terms_tr.
			'<table>';

		return $sites_terms_table;
	}


	// Term azonosítók törlése
	function remove_unused_terms_data_from_post_data($post_data = "") {
		$allowed_taxonomies = $this->wcs->functions->get_allowed_taxonomies();

		// Remove unused terms data
		foreach ($allowed_taxonomies as $key => $taxonomy) {
			if ( isset($post_data['term_'. $taxonomy]) ) {
				foreach ($post_data['term_'. $taxonomy] as $key => $term_obj) {
					if ( isset($post_data['term_'. $taxonomy][$key]->term_id) ) {
						unset($post_data['term_'. $taxonomy][$key]->term_id);
						unset($post_data['term_'. $taxonomy][$key]->term_group);
						unset($post_data['term_'. $taxonomy][$key]->term_taxonomy_id);
						unset($post_data['term_'. $taxonomy][$key]->parent);
						unset($post_data['term_'. $taxonomy][$key]->count);
						unset($post_data['term_'. $taxonomy][$key]->filter);
						unset($post_data['term_'. $taxonomy][$key]->description);
					}
				}
			}
		}

		return $post_data;
	}


	function get_featured_image_by_post_id($post_ID = "") {

		// if post_id is hash
		if ( !is_numeric($post_ID) && !empty($post_ID) ) {
			$post_ID = (int) $this->wcs->functions->get_post_id_by_post_hash($post_ID);
		} else {
			$post_ID = intval($post_ID);
		}


		if ( $post_ID > 0 ) {
			if ( has_post_thumbnail($post_ID) ) {
				$fullsize_path = get_attached_file( get_post_thumbnail_id($post_ID) );

				if ( file_exists($fullsize_path) ) {
					$file_content = file_get_contents($fullsize_path, false, NULL);

					if ( !empty($file_content) ) {
						return base64_encode( $file_content );
					}
				}
			}
		}

		return FALSE;
	}


	/**********************************************/
	/*     Send notifications to another site     */
	/**********************************************/


	// Ping küldése a másik oldalra
	function ping_to_other_site() {
		$other_site_url = $this->wcs->functions->get_other_site_url();

		if ( $other_site_url !== FALSE ) {
			$response = file_get_contents( $other_site_url. '/?ping=true&wcs_access_key='. $this->wcs->functions->get_access_key() );

			if ( !empty($response) && ($response !== FALSE) ) {
				$response = unserialize(base64_decode($response));
				if ( isset($response['result']) ) {
					return $response['result'];
				}
			}
		}
		return FALSE;
	}


	// Újragenerálja a másik oldalon a term hash azonosítókat
	function regenerate_terms_hash_other_site() {
		$other_site_url = $this->wcs->functions->get_other_site_url();

		if ( $other_site_url !== FALSE ) {
			$response = file_get_contents( $other_site_url. '/?regenerate_terms_hash=true&wcs_access_key='. $this->wcs->functions->get_access_key() );

			if ( !empty($response) && ($response !== FALSE) ) {
				$response = unserialize(base64_decode($response));
				if ( isset($response['ready']) && ($response['ready'] === TRUE) ) {
					return TRUE;
				}
			}
		}
		return FALSE;
	}


	// Újragenerálja a másik oldalon a post hash azonosítókat
	function regenerate_posts_hash_other_site() {
		$other_site_url = $this->wcs->functions->get_other_site_url();

		if ( $other_site_url !== FALSE ) {
			$response = file_get_contents( $other_site_url. '/?regenerate_posts_hash=true&wcs_access_key='. $this->wcs->functions->get_access_key() );

			if ( !empty($response) && ($response !== FALSE) ) {
				$response = unserialize(base64_decode($response));
				if ( isset($response['ready']) && ($response['ready'] === TRUE) ) {
					return TRUE;
				}
			}
		}
		return FALSE;
	}


	// Lekéri a másik oldalról a postok alap adatait
	function get_basic_posts_datas_from_other_site() {
		$other_site_url = $this->wcs->functions->get_other_site_url();

		if ( $other_site_url !== FALSE ) {
			$response = file_get_contents( $other_site_url. '/?show_basic_posts_datas=true&wcs_access_key='. $this->wcs->functions->get_access_key() );

			if ( !empty($response) && ($response !== FALSE) ) {
				$response = unserialize(base64_decode($response));
				if ( !empty($response) && is_array($response) ) {
					return $response;
				}
			}
		}
		return FALSE;
	}


	// Lekéri a másik oldalról a post összes adatát
	function get_post_datas_from_other_site($post_hash = "") {
		if ( empty($post_hash) ) { return FALSE; }
		$other_site_url = $this->wcs->functions->get_other_site_url();

		if ( $other_site_url !== FALSE ) {
			$response = file_get_contents( $other_site_url. '/?show_selected_post_datas=true&post_hash='. $post_hash .'&wcs_access_key='. $this->wcs->functions->get_access_key() );
			if ( !empty($response) && ($response !== FALSE) ) {
				$response = unserialize(base64_decode($response));
				if ( !empty($response) && is_array($response) ) {
					return $response;
				}
			}
		}
		return FALSE;
	}


	// Lekéri a másik oldalról az összes kategóriát
	function get_all_terms_from_other_site() {
		$other_site_url = $this->wcs->functions->get_other_site_url();

		if ( $other_site_url !== FALSE ) {
			$response = file_get_contents( $other_site_url. '/?get_all_terms=true&wcs_access_key='. $this->wcs->functions->get_access_key() );
			if ( !empty($response) && ($response !== FALSE) ) {
				$response = unserialize(base64_decode($response));
				if ( !empty($response) && is_array($response) ) {
					return $response;
				}
			}
		}
		return FALSE;
	}


	// Cache törlése a másik oldalon
	function clear_diff_site_posts_cache_other_site() {
		$other_site_url = $this->wcs->functions->get_other_site_url();

		if ( $other_site_url !== FALSE ) {
			$response = file_get_contents( $other_site_url. '/?clear_diff_site_posts_cache=true&wcs_access_key='. $this->wcs->functions->get_access_key() );
			if ( !empty($response) && ($response !== FALSE) ) {
				$response = unserialize(base64_decode($response));
				if ( isset($response['ready']) && ($response['ready'] === TRUE) ) {
					return TRUE;
				}
			}
		}
		return FALSE;
	}


}
