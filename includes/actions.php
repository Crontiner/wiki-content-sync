<?php

/***********************/
/*     ADD ACTIONS     */
/***********************/

class WCS_Actions {

	public function __construct($wcs) {
		$this->wcs = $wcs;
	}

	/******************************/
	/*     DEFINE EDITOR PAGE     */
	/******************************/

	public function SyncMenuSettingsMenuItem() {
		add_menu_page(
			'Wiki Sync',
			'Wiki Sync',
			'manage_options',
			'wcs_sync_page',
			array($this, 'sync_page_function')
		);
	}

	public function sync_page_function() {

		// Save Settings
		if ( isset($_POST['save_settings']) ) {
			if ( 	isset($_POST['access_key']) && !empty($_POST['access_key']) &&
						isset($_POST['site1']) && !empty($_POST['site1']) &&
						isset($_POST['site2']) && !empty($_POST['site2']) ) {

				update_option	('wcs_settings', array(
																							'access_key' => $_POST['access_key'],
																							'site1' => $_POST['site1'],
																							'site2' => $_POST['site2'],
																						)
																					);
			}
		}
		if ( isset($_POST['wcs_clear_local_cache']) ) {
			update_option( 'wcs_diff_site_posts_cache', "", false );
			update_option( 'wcs_diff_site_terms_cache', "", false );
			$this->wcs->functions->clear_diff_site_posts_cache_other_site();
		}


		$wcs_settings = get_option('wcs_settings');

		?>
		<div class="wrap inside">
			<h2>Wiki bejegyzések szinkronizálása</h2>
			<?php
			$diff_site_basic_posts_datas = array();
			$other_site_url = $this->wcs->functions->get_other_site_url();

			// Get cache
			$wcs_diff_site_posts_cache_timestamp = "";
			$wcs_diff_site_posts_cache = get_option( 'wcs_diff_site_posts_cache' );
			if ( !empty($wcs_diff_site_posts_cache) ) {
				$wcs_diff_site_posts_cache = unserialize(base64_decode($wcs_diff_site_posts_cache));

				if ( isset($wcs_diff_site_posts_cache['timestamp']) ) {
					if ( time() - intval($wcs_diff_site_posts_cache['timestamp']) < Wiki_Content_Sync::DIFF_SITE_POSTS_CACHE_LIFETIME ) {
						$wcs_diff_site_posts_cache_timestamp = intval($wcs_diff_site_posts_cache['timestamp']);
						$wcs_diff_site_posts_cache = $wcs_diff_site_posts_cache['result'];
					} else { $wcs_diff_site_posts_cache = ""; }
				} else { $wcs_diff_site_posts_cache = ""; }
			}
			?>


			<form action="" method="post" id="access-key-form-field">
				<label for="access_key">Access Key:</label>
				<input type="text" value="<?php echo $wcs_settings['access_key']; ?>" name="access_key" id="access_key" />
				<br>
				<label for="site1">Site 1:</label>
				<input type="text" value="<?php echo $wcs_settings['site1']; ?>" name="site1" id="site1" />
				<br>
				<label for="site2">Site 2:</label>
				<input type="text" value="<?php echo $wcs_settings['site2']; ?>" name="site2" id="site2" />
				<br>
				<input type="submit" value="Save" name="save_settings" class="button button-primary button-large" />

				<br><br>
				<?php if ( !empty($wcs_diff_site_posts_cache) ) { ?>
				<label style="width: 100%;">Cache Countdown: <?php echo round(intval(Wiki_Content_Sync::DIFF_SITE_POSTS_CACHE_LIFETIME - (time() - intval($wcs_diff_site_posts_cache_timestamp))) / 60); ?> min</label>
				<?php } ?>
				<div class="clear"></div>
				<input type="submit" value="Clear the cache" name="wcs_clear_local_cache" class="button button-primary button-large" />
			</form>


			<?php

				echo $this->wcs->functions->get_last_synchronized_posts_table();


				// Send Ping msg
				if ( $this->wcs->functions->ping_to_other_site() != 'pong' ) {
					echo '<p class="info-text">Nem sikerült csatlakozni a másik oldalhoz!</p>'; die;
				}


				if ( empty($wcs_diff_site_posts_cache) ) {

					// Regenerate Posts & Terms Hash
					$this->wcs->functions->generate_posts_hash();
					$this->wcs->functions->generate_terms_hash();
					$posts_hash_result = $this->wcs->functions->regenerate_posts_hash_other_site();
					$terms_hash_result = $this->wcs->functions->regenerate_terms_hash_other_site();
					// ---

					if ( ($posts_hash_result === TRUE) && ($terms_hash_result === TRUE) ) {

						$allowed_post_types 	= $this->wcs->functions->get_allowed_cpts();
						$this_site_posts 			= $this->wcs->functions->get_basic_posts_datas();
						$other_site_posts 		= $this->wcs->functions->get_basic_posts_datas_from_other_site();
						$sites_posts 					= array();


						if ( !empty($this_site_posts) && is_array($this_site_posts) ) {
							foreach ($allowed_post_types as $key => $post_type) {
								foreach ($this_site_posts[$post_type] as $key => $post_array) {
									if ( isset($post_array['slug']) ) {
										$post_array['site'] = 'this';
										$post_array['post_type'] = $post_type;
										$sites_posts[$post_type][$post_array['slug']][]= $post_array;
									}
								}
							}
						}

						if ( !empty($other_site_posts) && is_array($other_site_posts) ) {
							foreach ($allowed_post_types as $key => $post_type) {
								foreach ($other_site_posts[$post_type] as $key => $post_array) {
									if ( isset($post_array['slug']) ) {
										$post_array['site'] = 'other';
										$post_array['post_type'] = $post_type;
										$sites_posts[$post_type][$post_array['slug']][]= $post_array;
									}
								}
							}
						}


						// Remove not changed posts
						if ( !empty($sites_posts) && is_array($sites_posts) ) {
							foreach ($sites_posts as $post_type => $posts_array) {

								foreach ($sites_posts[$post_type] as $slug => $posts_array2) {
									if ( count($posts_array2) > 1 ) {

										if ( $sites_posts[$post_type][$slug][0]['wcs_post_data_hash'] == $sites_posts[$post_type][$slug][1]['wcs_post_data_hash'] ) {
											unset($sites_posts[$post_type][$slug]);
										} else {

											$sites_posts[$post_type][$slug][0]['duplicate'] = TRUE;
											$sites_posts[$post_type][$slug][1]['duplicate'] = TRUE;

											// remove old version
											if ( intval($sites_posts[$post_type][$slug][0]['post_modified_date']) > intval($sites_posts[$post_type][$slug][1]['post_modified_date']) ) {

												$sites_posts[$post_type][$slug][0]['other_site_post_id'] = $sites_posts[$post_type][$slug][1]['post_id'];
												$sites_posts[$post_type][$slug][0]['other_site_post_data_hash'] = $sites_posts[$post_type][$slug][1]['wcs_post_data_hash'];

												unset($sites_posts[$post_type][$slug][1]);
												$sites_posts[$post_type][$slug] = array_values($sites_posts[$post_type][$slug]);
											} else if ( intval($sites_posts[$post_type][$slug][0]['post_modified_date']) < intval($sites_posts[$post_type][$slug][1]['post_modified_date']) ) {

												$sites_posts[$post_type][$slug][1]['other_site_post_id'] = $sites_posts[$post_type][$slug][0]['post_id'];
												$sites_posts[$post_type][$slug][1]['other_site_post_data_hash'] = $sites_posts[$post_type][$slug][0]['wcs_post_data_hash'];

												unset($sites_posts[$post_type][$slug][0]);
												$sites_posts[$post_type][$slug] = array_values($sites_posts[$post_type][$slug]);
											} else {
												unset($sites_posts[$post_type][$slug]);
											}
										}
									} else {
										$sites_posts[$post_type][$slug][0]['duplicate'] = FALSE;
									}
								}
							}
						}


						// Sort by post_modified_date
						$sites_posts_temp = array();
						if ( !empty($sites_posts) && is_array($sites_posts) ) {
							foreach ($sites_posts as $post_type => $val) {
								foreach ($sites_posts[$post_type] as $slug => $posts_array) {
									$sites_posts_temp []= $posts_array[0];
								}
							}
						}
						$this->wcs->functions->array_sort_by_column($sites_posts_temp, 'post_modified_date');
						$sites_posts = array_reverse($sites_posts_temp);
						unset($sites_posts_temp);


						$diff_site_posts_array = array();
						foreach ($sites_posts as $key => $post_data) {
							if ( isset($post_data['site']) ) {
								if ( $post_data['site'] == 'this' ) {
									$diff_site_posts_array['this_site'][] = $post_data;
								} else {
									$diff_site_posts_array['other_site'][] = $post_data;
								}
							}
						}
						unset($sites_posts);
					}


					// Change other site post's IDs & Hash
					if ( isset($diff_site_posts_array['other_site']) && !empty($diff_site_posts_array['other_site']) ) {
						foreach ($diff_site_posts_array['other_site'] as $key => $post_data) {
							if ( $post_data['duplicate'] === TRUE ) {
								$post_id_temp = $post_data['post_id'];
								$other_site_post_id_temp = $post_data['other_site_post_id'];
								$wcs_post_data_hash_temp = $post_data['wcs_post_data_hash'];
								$other_site_post_data_hash_temp = $post_data['other_site_post_data_hash'];

								$diff_site_posts_array['other_site'][$key]['post_id'] = $other_site_post_id_temp;
								$diff_site_posts_array['other_site'][$key]['wcs_post_data_hash'] = $other_site_post_data_hash_temp;

								$diff_site_posts_array['other_site'][$key]['other_site_post_id'] = $post_id_temp;
								$diff_site_posts_array['other_site'][$key]['other_site_post_data_hash'] = $wcs_post_data_hash_temp;
							} else {
								$diff_site_posts_array['other_site'][$key]['post_id'] = "";
								$diff_site_posts_array['other_site'][$key]['wcs_post_data_hash'] = "";

								$diff_site_posts_array['other_site'][$key]['other_site_post_id'] = $post_data['post_id'];
								$diff_site_posts_array['other_site'][$key]['other_site_post_data_hash'] = $post_data['wcs_post_data_hash'];
							}
						}
					}


					// Set cache
					if ( !isset($diff_site_posts_array) ) {
						$diff_site_posts_array = "";
						echo '<p class="info-text">Nincs szinkronizálandó bejegyzés.</p>';
					}
					update_option( 'wcs_diff_site_posts_cache', base64_encode(serialize(array( 'result' => $diff_site_posts_array, 'timestamp' => time() ))), false );

				} else {

					// Use Cache
					$diff_site_posts_array = $wcs_diff_site_posts_cache;
				}


				if ( !empty($diff_site_posts_array) && is_array($diff_site_posts_array) ) {

					$this_site_table_tr = "";
					if ( isset($diff_site_posts_array['this_site']) ) {
						foreach ($diff_site_posts_array['this_site'] as $key => $post_data) {

							$this_site_table_tr .=
								'<tr data-this-post-id="'. $post_data['post_id'] .'" data-other-post-id="'. $post_data['other_site_post_id'] .'" data-this-post-hash="'. $post_data['wcs_post_data_hash'] .'" data-other-post-hash="'. $post_data['other_site_post_data_hash'] .'" data-duplicate="'. strtolower(json_encode($post_data['duplicate'])) .'">'.
									'<td><a href="'. get_edit_post_link($post_data['post_id']) .'" target="_blank">'. $post_data['post_title'] .'</a></td>'.
									'<td><a href="'. get_edit_post_link($post_data['post_id']) .'" target="_blank">'. $post_data['slug'] .'</a></td>'.
									'<td>'. strtoupper($post_data['post_type']) .'</td>'.
									'<td>'. date('Y-m-d H:i:s', $post_data['post_modified_date']) .'</td>'.
									'<td>'. strtoupper(json_encode($post_data['duplicate'])) .'</td>'.
									'<td>'. '<button class="get_post_datas button button-default button-large">Részletek</button>' .'</td>'.
								'</tr>';
						}
					}

					$other_site_table_tr = "";
					if ( isset($diff_site_posts_array['other_site']) ) {
						foreach ($diff_site_posts_array['other_site'] as $key => $post_data) {

							$other_site_table_tr .=
								'<tr data-this-post-id="'. $post_data['post_id'] .'" data-other-post-id="'. $post_data['other_site_post_id'] .'" data-this-post-hash="'. $post_data['wcs_post_data_hash'] .'" data-other-post-hash="'. $post_data['other_site_post_data_hash'] .'" data-duplicate="'. strtolower(json_encode($post_data['duplicate'])) .'">'.
									'<td><a href="'. $other_site_url .'/wp-admin/post.php?post='. $post_data['other_site_post_id'] .'&amp;action=edit" target="_blank">'. $post_data['post_title'] .'</a></td>'.
									'<td><a href="'. $other_site_url .'/wp-admin/post.php?post='. $post_data['other_site_post_id'] .'&amp;action=edit" target="_blank">'. $post_data['slug'] .'</a></td>'.
									'<td>'. strtoupper($post_data['post_type']) .'</td>'.
									'<td>'. date('Y-m-d H:i:s', $post_data['post_modified_date']) .'</td>'.
									'<td>'. strtoupper(json_encode($post_data['duplicate'])) .'</td>'.
									'<td>'. '<button class="get_post_datas button button-default button-large">Részletek</button>' .'</td>'.
								'</tr>';
						}
					}

					?>
					<table id="this_site_posts">
						<tr>
							<th class="main_header" colspan="6">This site posts - (<?php echo count($diff_site_posts_array['this_site']); ?> db)</th>
						</tr>
						<tr>
							<th>Post Title</th>
							<th>Post Slug</th>
							<th>Post Type</th>
							<th>Last modif. date</th>
							<th>Duplicate</th>
							<th>Get Details</th>
						</tr>
						<?php echo $this_site_table_tr; ?>
					</table>

					<table id="other_site_posts">
						<tr>
							<th class="main_header" colspan="6">Other site posts - (<?php echo count($diff_site_posts_array['other_site']); ?> db)</th>
						</tr>
						<tr>
							<th>Post Title</th>
							<th>Post Slug</th>
							<th>Post Type</th>
							<th>Last modif. date</th>
							<th>Duplicate</th>
							<th>Get Details</th>
						</tr>
						<?php echo $other_site_table_tr; ?>
					</table>
					<?php
				}
			?>

			<br>
			<?php
			echo $this->wcs->functions->get_modified_and_new_categories_table();
			?>

		</div>
		<?php
	}

	/* --- /DEFINE EDITOR PAGE --- */


	/*************************/
	/*     ADMIN NOTICES     */
	/*************************/

	public function postSynchronizationStatus() {
		$screen = get_current_screen();
		$allowed_post_types = $this->wcs->functions->get_allowed_cpts();

		$post_ID = get_the_ID();
		$post_slug = get_post_field('post_name', $post_ID);
		$post_modified_date = get_post_modified_time('U', false, $post_ID);
		$wcs_post_data_hash = get_post_meta($post_ID, 'wcs_post_data_hash', true);


		if ( in_array($screen->base, $allowed_post_types) ) {

			// Send Ping msg
			if ( $this->wcs->functions->ping_to_other_site() != 'pong' ) {
				?>
				<div class="notice notice-error is-dismissible">
					<p>Nem sikerült csatlakozni a másik oldalhoz!</p>
				</div>
				<?php
			} else {
				if ( !empty($post_slug) && (intval($post_modified_date) > 0) && !empty($wcs_post_data_hash) ) {

					// If should be synchronized...
					$other_site_post_datas = $this->wcs->functions->get_post_datas_from_other_site($post_slug);

					if ( isset($other_site_post_datas['wcs_post_data_hash']) ) {
						if ( 	($other_site_post_datas['wcs_post_data_hash'] == $wcs_post_data_hash) ||
									($other_site_post_datas['post_modified_date'] == $post_modified_date)
							 ) {
							?>
							<div class="notice notice-success is-dismissible">
								<p>Nem kell szinkronizálni a bejegyzést!</p>
							</div>
							<?php
						} else {
							?>
							<div class="notice notice-info is-dismissible">
								<p>Szinkronizálni lehet bejegyzést!</p>
							</div>
							<?php
						}
					}
				}
			}
		}
	}

	/* --- /ADMIN NOTICES --- */

}
