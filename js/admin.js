jQuery(function($) {

	// Get post datas

	$('body.toplevel_page_wcs_sync_page table tr td button.get_post_datas').click(function(e) {
		e.preventDefault();
		var $this = $(this);
		var $tr = $this.closest('tr');
		var thisSitePostHash = $tr.attr('data-this-post-hash');
		var otherSitePostHash = $tr.attr('data-other-post-hash');
		var duplicate = $tr.attr('data-duplicate');
		var ajaxHash = Math.floor((Math.random() * 99999) + 1);

		// Disable button
		$this.prop("disabled", true);

		var thisOrOther = 'this';
		if ( $this.closest('table').attr('id') == 'other_site_posts' ) { thisOrOther = 'other'; }

		$('<tr class="ajax-content" data-this-post-hash="'+ thisSitePostHash +'" data-other-post-hash="'+ otherSitePostHash +'" data-ajax-hash="'+ ajaxHash +'" data-duplicate="'+ duplicate +'" ><td colspan="6">Loading...</td></tr>').insertAfter($tr);


		jQuery.post(
			wcs.ajaxurl,
			{
				ajax_hash : ajaxHash,
				this_site_post_hash : thisSitePostHash,
				other_site_post_hash : otherSitePostHash,
				duplicate : duplicate,
				site : thisOrOther,
				action : 'get_post_datas_with_ajax'
			},
			function(data) {
				if (data.ajax_hash == ajaxHash) {
					$('tr.ajax-content[data-ajax-hash="'+ ajaxHash +'"] td').html( data.html );
				}
			},'json'
		);

	});


	// Start Sync
	$('body').on('click', 'tr.ajax-content button.start_sync', function(e){
		e.preventDefault();
		var $this = $(this);
		var $tr = $this.closest('tr');
		var thisSitePostHash = $tr.attr('data-this-post-hash');
		var otherSitePostHash = $tr.attr('data-other-post-hash');
		var duplicate = $tr.attr('data-duplicate');
		var ajaxHash = Math.floor((Math.random() * 99999) + 1);

		// Disable button
		$this.prop("disabled", true);
		$this.html('Syncing...');

		var thisOrOther = 'this';
		if ( $this.closest('table').attr('id') == 'other_site_posts' ) { thisOrOther = 'other'; }


		jQuery.post(
			wcs.ajaxurl,
			{
				ajax_hash : ajaxHash,
				this_site_post_hash : thisSitePostHash,
				other_site_post_hash : otherSitePostHash,
				duplicate : duplicate,
				site : thisOrOther,
				action : 'start_sync_with_ajax'
			},
			function(data) {
				if (data.ajax_hash == ajaxHash) {
					$this.html('Sync is done!');
				}
			},'json'
		);

	});


	// Skip Sync
	$('body').on('click', 'tr.ajax-content button.skip_sync', function(e){
		e.preventDefault();
		var $this = $(this);
		var $tr = $this.closest('tr');
		var thisSitePostHash = $tr.attr('data-this-post-hash');
		var otherSitePostHash = $tr.attr('data-other-post-hash');
		var duplicate = $tr.attr('data-duplicate');
		var ajaxHash = Math.floor((Math.random() * 99999) + 1);

		// Disable button
		$this.prop("disabled", true);
		$this.html('Loading...');

		var thisOrOther = 'this';
		if ( $this.closest('table').attr('id') == 'other_site_posts' ) { thisOrOther = 'other'; }


		jQuery.post(
			wcs.ajaxurl,
			{
				ajax_hash : ajaxHash,
				this_site_post_hash : thisSitePostHash,
				other_site_post_hash : otherSitePostHash,
				duplicate : duplicate,
				site : thisOrOther,
				action : 'skip_sync_with_ajax'
			},
			function(data) {
				if (data.ajax_hash == ajaxHash) {
					if (data.result === true) {
						$this.html('Skipping is done!');
					} else {
						$this.html('Error!');
					}
				}
			},'json'
		);

	});



});
